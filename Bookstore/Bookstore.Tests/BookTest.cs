﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Bookstore.Tests
{
    [TestClass]
    public class BookTest
    {
        [TestMethod]
        public void Test()
        {
            Console.WriteLine(Environment.CurrentDirectory);
        }

        [TestMethod]
        public void TestParseShort()
        {
            string tooshort = "";
        }

        [TestMethod]
        [ExpectedException(typeof(BookstoreException))]
        public void TestParseNoIsbn()
        {
            Book b = new Book();
            string noisbn = "Reading for Diversity and Social Justice 	'Adams, Maurianne'	Spring 2015	EDUC 2120	1	BATES	13442	Required	8	8	8	999999	258.45	193.85	122.08	91.99	No description available.";

            //Assert.
            b.ParseData(noisbn);
        }

        [TestMethod]
        public List<string> ReadFile(string filename)
        {
            List<string> things = new List<string>();
            using (StreamReader file = new StreamReader(filename))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (things.Count == 0 || line.StartsWith("978"))
                    {
                        things.Add(line);
                    }
                    else
                    {
                        things[things.Count - 1] = things[things.Count - 1] + line;
                    }
                }
            }
            return things;
        }

        [TestMethod]
        public void TestParseAccounts()
        {
            AccountManager account = AccountManager.GetInstance();

            Console.WriteLine(account.Login("oprestri", "mypass") == null);
        }
    }
}
