﻿using System;

namespace Bookstore
{
    public class BookstoreException : Exception
{
    public BookstoreException() { }
    public BookstoreException(string message) : base(message) { }
    public BookstoreException(string message, Exception e) : base(message, e) { }
}
}
