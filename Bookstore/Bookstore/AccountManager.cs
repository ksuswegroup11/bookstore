﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Bookstore
{
    public class AccountManager
    {
        //Handles everything involving accounts, including logging in.
        private static AccountManager instance;

        public List<Account> accounts { get; }
        public Account guest;

        //The account that is currently logged in
        private Account currentAccount;
        public Account CurrentAccount { get { return currentAccount; } }

        private string filename = HostingEnvironment.ApplicationPhysicalPath + "BookstoreData/accounts.txt";

        public AccountManager()
        {
            accounts = new List<Account>();
            guest = new Account();
            ReadAccountFile(filename);

            //currentAccount = guest;
        }

        public static AccountManager GetInstance()
        {
            if(instance == null)
            {
                instance = new AccountManager();
            }

            return instance;
        }

        //Read account data from file
        public void ReadAccountFile(string filename)
        {
            try
            {
                using (StreamReader reader = new StreamReader(filename))
                {
                    string line;
                    while((line = reader.ReadLine()) != null)
                    {
                        accounts.Add(new Account(line));
                    }
                }
            }
            catch(BookstoreException e)
            {
                
            }
            catch (FileNotFoundException e)
            {
            
            }
        }

        //Attempt to log into one of the listed accounts
        public Account Login(string id, string pass)
        {
            foreach(Account a in accounts)
            {
                if(id == a.NetID && pass == a.Password)
                {
                    currentAccount = a;
                    return a;
                }
            }

            currentAccount = null;
            return null;
        }

        public Account LoginGuest()
        {
            currentAccount = guest;
            return currentAccount;
        }

        //Log out (set the current account to null)
        public void Logout()
        {
            if(currentAccount == guest)
            {
                guest.RefreshCardData();
                currentAccount.Cart.EmptyCart();
            }
            currentAccount = null;
        }
    }
}
