﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookstore
{
    public class Account
    {
        private string netID;
        private string password;
        private string email;
        private string name;
        private string cardNumber;
        private string csc;
        private string expDate;
        private string address;

        private ShoppingCart cart;

        public string NetID { get { return netID; } }
        public string Password { get { return password; } }

        public string Email { get { return email; } set { email = value; } }
        public string Name { get { return name; } set { name = value; } }
        public string CardNumber { get { return cardNumber; } set { cardNumber = value; } }
        public string CSC { get { return csc; } set { csc = value; } }
        public string ExpDate { get { return expDate; } set { expDate = value; } }
        public string Address { get { return address; } set { address = value; } }
        
        public ShoppingCart Cart { get { return cart; }  set { cart = value; }}
    
        // Guest account
        public Account (){
            netID      = "guest";
            password   = null;
            email      = null;
            name       = null;
            cardNumber = null;
            csc        = null;
            expDate    = null;
            address    = null;
            cart = new ShoppingCart();
        }

        public Account(string data)
        {
            string[] splitData = data.Split('\t');

            netID = splitData[0];
            password = splitData[1];
            email = splitData[2];
            name = splitData[3];
            cardNumber = splitData[4];
            csc = splitData[5];
            expDate = splitData[6];
            address = splitData[7];

            cart = new ShoppingCart();
        }

        public bool isValidCardNumber()
        {
            return (cardNumber.Length == 16 && cardNumber.All(char.IsDigit));
        }

        public bool isValidCSC()
        {
            return csc == "777";
        }

        public bool isValidExpDate()
        {
            DateTime date = DateTime.Parse(expDate);
            return (date > DateTime.Today);
        }

        public void placeOrder()
        {
            Order o = new Order(this);
            o.RecordReceipt();
        }

        public void RefreshCardData()
        {
            email = null;
            name = null;
            cardNumber = null;
            csc = null;
            expDate = null;
            address = null;
        }
    }
}
