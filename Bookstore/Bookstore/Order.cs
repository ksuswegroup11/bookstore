using System;
using System.IO;
using System.Web.Hosting;
namespace Bookstore
{
    public class Order { 

        private static int ordernumber = 0;
        private string SaveDir = HostingEnvironment.ApplicationPhysicalPath + "/Receipts/";

            
        private int orderid;
        private Account account;
        private ShoppingCart cart;

        public Order(Account a){
            account = a;
            cart = a.Cart;
            // Cleverly give us a 'unique' order number
            // TODO: Make this be constant across restarts
            this.orderid = ordernumber;
            ordernumber++;
        }

        public void RecordReceipt() {
            DateTime today = DateTime.Now;
            using (StreamWriter f = 
                    new StreamWriter(SaveDir + orderid.ToString() + " " + today.ToString("MM-dd-yyyy HH-mm") + ".txt"))
            {
                f.WriteLine(account.Name);
                f.WriteLine(account.Address);
                // Iterate over the books and print their ISBNs (and other info if needed)
                for (int i = 0; i < cart.Books.Count; i++){
                    f.WriteLine(cart.Books[i].Info[(int)Book.BookInfo.Isbn] + ", " + 
                                cart.booktypes[i]);
                }
            }
        }
    }
}
