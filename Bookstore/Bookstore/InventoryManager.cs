﻿using System.Collections.Generic;
using System.IO;
using System;
using System.Web.Hosting;

namespace Bookstore{

    /// <summary>
    /// Summary description for Class1
    /// </summary>
    public class InventoryManager
    {
        private static InventoryManager instance;
        private string head;
        public List<Book> books { get; set; }
        private string filename = HostingEnvironment.ApplicationPhysicalPath + "BookstoreData/thedata.txt";


        public InventoryManager()
        {
            books = new List<Book>();
            List<string> fileData = ReadFile(filename);

            head = fileData[0];

            for(int i = 1; i < fileData.Count; i++)
            {
                books.Add(new Book(fileData[i]));
            }
        }

        // Singleton so we don't have weird multiples across files
        public static InventoryManager GetInstance()
        {
            if (instance == null)
            {
                instance = new InventoryManager();
            }
            return instance;
        }

        //Takes a file and converts the data into a list of strings to be later converted into book data
        public List<string> ReadFile(string filename){
            List<string> things = new List<string>();
            try
            {
                using (StreamReader file = new StreamReader(filename))
                {
                    string line;
                    while ((line = file.ReadLine()) != null)
                    {
                        //Add the line to the list if the list is empty or the string starts with an ISBN number
                        if (things.Count == 0 || line.StartsWith("978"))
                        {
                            things.Add(line);
                        }

                        //Otherwise, add the line to the previous string in the list
                        else
                        {
                            things[things.Count - 1] = things[things.Count - 1] + line;
                        }
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                Console.Out.WriteLine("Could not find data file; expected " + e.FileName);
            }

            return things;
        }

        //Searches for books that match the given criteria and returns a list of those books
        public List<Book> Filter(string s, Book.BookInfo type)
        {
            List<Book> filteredBooks = new List<Book>();

            foreach(Book b in books)
            {
                if(b.Contains(s, type))
                {
                    filteredBooks.Add(b);
                }
            }

            return filteredBooks;
        }

        //Update the inventory spreadsheet by overwriting the spreadsheet file
        public void Update()
        {
            using (StreamWriter file = new StreamWriter(filename, false))
            {
                file.WriteLine(head);

                foreach(Book b in books)
                {
                    file.WriteLine(b.ToString());
                }
            }
        }

        public void DumpInventory(){
        }
    }

}
