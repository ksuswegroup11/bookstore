﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore
{
    public class Book
    {
        //Used to differentiate the different prices and quantities of the same book.
        //Refer to this when accessing or modifying the price and quantity arrays
        public enum BookType
        {
            New, Used, Rent, Ebook
        }

        //Used to specify book information when searching and acquiring information on the book
        public enum BookInfo
        {
            Isbn, Title, Author, Semester, Course, Section, Professor, Crn, Usage
        }

        //Most of the book information goes here (refer to BookInfo)
        private string[] info;

        private int[] quantities;
        private float[] prices;
        private string description;

        public Book()
        {
            // Default values (that make sense)
            this.info = new string[9];
            this.info[(int)BookInfo.Isbn] = "0";
            this.info[(int)BookInfo.Title] = "Title not available";
            this.info[(int)BookInfo.Author] = "Author not available";
            this.info[(int)BookInfo.Semester] = "Semester not available";
            this.info[(int)BookInfo.Course] = "Course not available";
            this.info[(int)BookInfo.Section] = "Section not available";
            this.info[(int)BookInfo.Professor] = "Professor not available";
            this.info[(int)BookInfo.Crn] = "CRN not available";
            this.info[(int)BookInfo.Usage] = "Usage not available";
        }

        public Book(string bookData) : this()
        {
            this.ParseData(bookData);
        }

        //Takes a string of data and converts it to the book information
        public void ParseData(string bookData)
        {
            //Splits the string into an array, then copies the values into each respective array
            string[] splitData = bookData.Split('\t');
            Debug.WriteLine((splitData[splitData.Length - 1]) == "");

            info = new string[9];
            Array.Copy(splitData, info, 9);

            quantities = new int[4];
            for(int i = 9; i < 13; i++)
            {
                quantities[i - 9] = (splitData[i] == "inf") ? 99999 : int.Parse(splitData[i]);
            }
            
            prices = new float[4];
            for (int i = 13; i < 17; i++)
            {
                prices[i - 13] = float.Parse(splitData[i]);
            }

            description = splitData[17];
        }

        // Getters and setters
        public string[] Info {  get { return info; } }
        public string Description { get { return description; } }

        public float GetPrice(BookType type)
        {
            return prices[(int)type];
        }

        public int GetQuantity(BookType type)
        {
            return quantities[(int)type];
        }

        //Takes a string and compares it to specific book info based on the search type.
        public bool Contains(string s, Book.BookInfo type)
        {
            // What are we comparing the string to?
            string compare = this.Info[(int)type];
            string lowercomp = compare.ToLower();
            return compare.Contains(s) || lowercomp.Contains(s.ToLower());
        }

        //Lowers the quantity of a specific book type by 1.
        public void Decrement(Book.BookType type)
        {
            quantities[(int)type]--;
        }

        //Increases the quantity of a specific book type by 1.
        public void Increment(Book.BookType type)
        {
            quantities[(int)type]++;
        }

        //Takes a type of book and checks to see if there are books of that type available
        public bool IsAvailable(BookType type)
        {
            return (quantities[(int)type] > 0);
        }

        //Converts all of the book data into a single string. Used to update the spreadsheet
        override public string ToString()
        {
            string s = "";

            foreach(string str in info)
            {
                s = s + str + "\t";
            }

            foreach(int i in quantities)
            {
                s = s + i.ToString() + "\t";
            }

            foreach(float f in prices)
            {
                s = s + f.ToString() + "\t";
            }

            s = s + description;

            return s;
        }
    }
}
