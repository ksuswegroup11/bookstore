using System.Collections.Generic;

namespace Bookstore
{
    public class ShoppingCart
    {
        public List<Book> Books { get; }
        public List<Book.BookType> booktypes;

        public ShoppingCart (){
            Books = new List<Book>();
            booktypes = new List<Book.BookType>();
        }

        /// <summary>
        /// Get the current grand total of the prices of the items in the cart.
        /// </summary>
        public float GetTotal(){
            float total = 0;
            for (int i = 0; i < Books.Count; i++)
            {
                total = total + Books[i].GetPrice(booktypes[i]);
            }
            return total;
        }

        public void AddBook(Book b, Book.BookType type){
            Books.Add(b);
            booktypes.Add(type);
        }

        public void RemoveBook(int index){
            Books.RemoveAt(index);
            booktypes.RemoveAt(index);
        }
        
        public void EmptyCart()
        {
            Books.Clear();
        }
    }
}
